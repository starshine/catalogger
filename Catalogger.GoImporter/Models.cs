// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using NodaTime;
using Remora.Discord.API.Objects;
using J = System.Text.Json.Serialization.JsonPropertyNameAttribute;

// ReSharper disable NotAccessedPositionalProperty.Global

namespace Catalogger.GoImporter;

public record GoGuild(
    [property: J("ID")] ulong Id,
    Dictionary<string, string> Channels,
    Dictionary<string, string> Redirects,
    ulong[] IgnoredChannels,
    string[] BannedSystems,
    ulong[] KeyRoles,
    ulong[] IgnoredUsers
);

public record GoInvite([property: J("GuildID")] ulong GuildId, string Code, string Name);

public record GoWatchlistEntry(
    [property: J("GuildID")] ulong GuildId,
    [property: J("UserID")] ulong UserId,
    [property: J("Moderator")] ulong ModeratorId,
    [property: J("Added")] Instant AddedAt,
    string Reason
);

public record GuildsExport(
    List<GoGuild> Guilds,
    List<GoInvite> Invites,
    List<GoWatchlistEntry> Watchlist
);

public record GoMessage(
    [property: J("MsgID")] ulong Id,
    [property: J("UserID")] ulong UserId,
    [property: J("ChannelID")] ulong ChannelId,
    [property: J("ServerID")] ulong GuildId,
    string Content,
    string Username,
    string? Member,
    string? System,
    GoMetadata? Metadata
);

public record GoMetadata(
    [property: J("UserID")] ulong? UserId,
    string? Username,
    string? Avatar,
    EmbedField[]? Embeds
);
