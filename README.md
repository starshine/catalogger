# Catalogger.NET

C# rewrite of [Catalogger][old-repo].

## Development

Command-line tools for this project can be installed with `dotnet tool restore`.

- We use [Husky.Net][husky] for Git hooks, install it with `dotnet husky install`.
- We use [CSharpier][csharpier] for formatting .NET code.
  It can be called with `dotnet csharpier .`, but is automatically run by Husky pre-commit.

### Nuget

We currently use Remora's GitHub packages as the releases on nuget.org are missing some key features.
Add these with `dotnet nuget add source --username <githubUsername> --password <githubToken> --store-password-in-clear-text --name Remora "https://nuget.pkg.github.com/Remora/index.json"`

You must generate a personal access token (classic) [here](personal-access-token). Only give it the `read:packages` permission.

## Deploying Catalogger yourself

The bot itself should run on any server with .NET 8 and PostgreSQL 15 or later.
A Redis-compatible database is *not* a hard dependency for the bot,
but may be used for faster restarts (when Redis is used, certain data will be cached there instead of in memory).

The dashboard isn't made for self-hosting. While it *should* work, the about/contact/ToS/privacy policy pages are hardcoded for our own convenience.

For now, you'll also have to follow the instructions in the "Nuget" section above.

Steps to build and run the bot:

1. Clone the repository
2. Run `dotnet publish` ([documentation][dotnet-publish])
3. Change directory into `Catalogger.Backend/`
4. Copy `config.example.ini` to `config.ini` and fill it out.
5. Run the bot by executing `bin/Release/net8.0/Catalogger.Backend` (or net9.0 if you're using that version)

## License

Copyright (C) 2021-present sam (https://starshines.gay)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

[old-repo]: https://github.com/starshine-sys/catalogger/tree/main
[husky]: https://github.com/alirezanet/Husky.Net
[csharpier]: https://csharpier.com/
[personal-access-token]: https://github.com/settings/tokens
[dotnet-publish]: https://learn.microsoft.com/en-us/dotnet/core/deploying/