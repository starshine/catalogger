// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using NodaTime;
using Prometheus;

namespace Catalogger.Backend;

public static class CataloggerMetrics
{
    public static Instant Startup { get; set; }

    public static readonly Gauge MessagesReceived = Metrics.CreateGauge(
        "catalogger_received_messages",
        "Number of messages Catalogger has received"
    );

    public static long MessageRateMinute { get; set; }

    public static readonly Gauge DatabaseConnections = Metrics.CreateGauge(
        "catalogger_open_database_connections",
        "Number of open database connections"
    );

    public static readonly Gauge GuildsCached = Metrics.CreateGauge(
        "catalogger_cache_guilds",
        "Number of guilds in the cache"
    );

    public static readonly Gauge ChannelsCached = Metrics.CreateGauge(
        "catalogger_cache_channels",
        "Number of channels in the cache"
    );

    public static readonly Gauge RolesCached = Metrics.CreateGauge(
        "catalogger_cache_roles",
        "Number of roles in the cache"
    );

    public static readonly Gauge UsersCached = Metrics.CreateGauge(
        "catalogger_cache_users",
        "Number of users in the cache"
    );

    public static readonly Gauge EmojiCached = Metrics.CreateGauge(
        "catalogger_cache_emoji",
        "Number of custom emoji in the cache"
    );

    public static readonly Gauge MessagesStored = Metrics.CreateGauge(
        "catalogger_stored_messages",
        "Number of users in the cache"
    );

    public static readonly Summary MetricsCollectionTime = Metrics.CreateSummary(
        "catalogger_time_metrics",
        "Time it took to collect metrics"
    );

    public static Gauge ProcessPhysicalMemory =>
        Metrics.CreateGauge("catalogger_process_physical_memory", "Process physical memory");

    public static Gauge ProcessVirtualMemory =>
        Metrics.CreateGauge("catalogger_process_virtual_memory", "Process virtual memory");

    public static Gauge ProcessPrivateMemory =>
        Metrics.CreateGauge("catalogger_process_private_memory", "Process private memory");

    public static Gauge ProcessThreads =>
        Metrics.CreateGauge("catalogger_process_threads", "Process thread count");

    public static Gauge ProcessHandles =>
        Metrics.CreateGauge("catalogger_process_handles", "Process handle count");
}
