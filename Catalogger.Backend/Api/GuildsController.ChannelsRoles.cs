// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Microsoft.AspNetCore.Mvc;
using Remora.Discord.API.Abstractions.Objects;

namespace Catalogger.Backend.Api;

public partial class GuildsController
{
    [HttpPut("ignored-messages/channels/{channelId}")]
    public async Task<IActionResult> AddIgnoredMessageChannelAsync(string id, ulong channelId)
    {
        var (guildId, _) = await ParseGuildAsync(id);
        var guildConfig = await guildRepository.GetAsync(guildId);

        if (guildConfig.Messages.IgnoredChannels.Contains(channelId))
            return NoContent();

        var channel = channelCache
            .GuildChannels(guildId)
            .FirstOrDefault(c =>
                c.ID.Value == channelId
                && c.Type
                    is ChannelType.GuildText
                        or ChannelType.GuildCategory
                        or ChannelType.GuildAnnouncement
                        or ChannelType.GuildForum
                        or ChannelType.GuildMedia
                        or ChannelType.GuildVoice
            );
        if (channel == null)
            return NoContent();

        guildConfig.Messages.IgnoredChannels.Add(channelId);
        await guildRepository.UpdateConfigAsync(guildId, guildConfig);

        return NoContent();
    }

    [HttpDelete("ignored-messages/channels/{channelId}")]
    public async Task<IActionResult> RemoveIgnoredMessageChannelAsync(string id, ulong channelId)
    {
        var (guildId, _) = await ParseGuildAsync(id);
        var guildConfig = await guildRepository.GetAsync(guildId);

        guildConfig.Messages.IgnoredChannels.Remove(channelId);
        await guildRepository.UpdateConfigAsync(guildId, guildConfig);

        return NoContent();
    }

    [HttpPut("ignored-messages/roles/{roleId}")]
    public async Task<IActionResult> AddIgnoredMessageRoleAsync(string id, ulong roleId)
    {
        var (guildId, _) = await ParseGuildAsync(id);
        var guildConfig = await guildRepository.GetAsync(guildId);

        if (guildConfig.Messages.IgnoredRoles.Contains(roleId))
            return NoContent();

        if (roleCache.GuildRoles(guildId).All(r => r.ID.Value != roleId))
            return NoContent();

        guildConfig.Messages.IgnoredRoles.Add(roleId);
        await guildRepository.UpdateConfigAsync(guildId, guildConfig);

        return NoContent();
    }

    [HttpDelete("ignored-messages/roles/{roleId}")]
    public async Task<IActionResult> RemoveIgnoredMessageRoleAsync(string id, ulong roleId)
    {
        var (guildId, _) = await ParseGuildAsync(id);
        var guildConfig = await guildRepository.GetAsync(guildId);

        guildConfig.Messages.IgnoredRoles.Remove(roleId);
        await guildRepository.UpdateConfigAsync(guildId, guildConfig);

        return NoContent();
    }

    [HttpPut("ignored-entities/channels/{channelId}")]
    public async Task<IActionResult> AddIgnoredEntityChannelAsync(string id, ulong channelId)
    {
        var (guildId, _) = await ParseGuildAsync(id);
        var guildConfig = await guildRepository.GetAsync(guildId);

        if (guildConfig.IgnoredChannels.Contains(channelId))
            return NoContent();

        var channel = channelCache
            .GuildChannels(guildId)
            .FirstOrDefault(c =>
                c.ID.Value == channelId
                && c.Type
                    is ChannelType.GuildText
                        or ChannelType.GuildCategory
                        or ChannelType.GuildAnnouncement
                        or ChannelType.GuildForum
                        or ChannelType.GuildMedia
                        or ChannelType.GuildVoice
            );
        if (channel == null)
            return NoContent();

        guildConfig.IgnoredChannels.Add(channelId);
        await guildRepository.UpdateConfigAsync(guildId, guildConfig);

        return NoContent();
    }

    [HttpDelete("ignored-entities/channels/{channelId}")]
    public async Task<IActionResult> RemoveIgnoredEntityChannelAsync(string id, ulong channelId)
    {
        var (guildId, _) = await ParseGuildAsync(id);
        var guildConfig = await guildRepository.GetAsync(guildId);

        guildConfig.IgnoredChannels.Remove(channelId);
        await guildRepository.UpdateConfigAsync(guildId, guildConfig);

        return NoContent();
    }

    [HttpPut("ignored-entities/roles/{roleId}")]
    public async Task<IActionResult> AddIgnoredEntityRoleAsync(string id, ulong roleId)
    {
        var (guildId, _) = await ParseGuildAsync(id);
        var guildConfig = await guildRepository.GetAsync(guildId);

        if (guildConfig.Messages.IgnoredRoles.Contains(roleId))
            return NoContent();

        if (roleCache.GuildRoles(guildId).All(r => r.ID.Value != roleId))
            return NoContent();

        guildConfig.IgnoredRoles.Add(roleId);
        await guildRepository.UpdateConfigAsync(guildId, guildConfig);

        return NoContent();
    }

    [HttpDelete("ignored-entities/roles/{roleId}")]
    public async Task<IActionResult> RemoveIgnoredEntityRoleAsync(string id, ulong roleId)
    {
        var (guildId, _) = await ParseGuildAsync(id);
        var guildConfig = await guildRepository.GetAsync(guildId);

        guildConfig.IgnoredRoles.Remove(roleId);
        await guildRepository.UpdateConfigAsync(guildId, guildConfig);

        return NoContent();
    }
}
