// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace Catalogger.Backend.Api;

public record ApiUser(string Id, string Tag, string AvatarUrl)
{
    public ApiUser(User baseUser)
        : this(baseUser.Id, baseUser.Tag, baseUser.AvatarUrl) { }
}

public record ApiGuild(string Id, string Name, string IconUrl, bool BotInGuild)
{
    public ApiGuild(Guild baseGuild, bool botInGuild)
        : this(baseGuild.Id, baseGuild.Name, baseGuild.IconUrl, botInGuild) { }
}
