// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Catalogger.Backend.Api.Middleware;
using Catalogger.Backend.Cache.InMemoryCache;
using Catalogger.Backend.Services;
using Microsoft.AspNetCore.Mvc;

namespace Catalogger.Backend.Api;

[Route("/api")]
public class MetaController(
    Config config,
    GuildCache guildCache,
    NewsService newsService,
    DiscordRequestService discordRequestService
) : ApiControllerBase
{
    [HttpGet("meta")]
    public async Task<IActionResult> GetMetaAsync()
    {
        var inviteUrl =
            $"https://discord.com/oauth2/authorize?client_id={config.Discord.ApplicationId}"
            + "&permissions=537250993&scope=bot+applications.commands";

        var news = await newsService.GetNewsAsync();

        return Ok(
            new MetaResponse(
                Guilds: (int)CataloggerMetrics.GuildsCached.Value,
                InviteUrl: inviteUrl,
                News: news
            )
        );
    }

    [HttpGet("current-user")]
    [Authorize]
    public async Task<IActionResult> GetCurrentUserAsync()
    {
        var token = HttpContext.GetTokenOrThrow();

        var currentUser = await discordRequestService.GetMeAsync(token);
        var guilds = await discordRequestService.GetGuildsAsync(token);

        return Ok(
            new CurrentUserResponse(
                new ApiUser(currentUser),
                guilds
                    .Where(g => g.CanManage)
                    .Select(g => new ApiGuild(g, guildCache.Contains(g.Id)))
            )
        );
    }

    [HttpGet("coffee")]
    public IActionResult BrewCoffee() =>
        Problem("Sorry, I'm a teapot!", statusCode: StatusCodes.Status418ImATeapot);

    private record MetaResponse(
        int Guilds,
        string InviteUrl,
        IEnumerable<NewsService.NewsMessage> News
    );

    private record CurrentUserResponse(ApiUser User, IEnumerable<ApiGuild> Guilds);
}
