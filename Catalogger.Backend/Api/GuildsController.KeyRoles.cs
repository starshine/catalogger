// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Net;
using Catalogger.Backend.Api.Middleware;
using Microsoft.AspNetCore.Mvc;
using Remora.Discord.API;

namespace Catalogger.Backend.Api;

public partial class GuildsController
{
    [HttpPut("key-roles/{roleId}")]
    public async Task<IActionResult> AddKeyRoleAsync(string id, ulong roleId)
    {
        var (guildId, _) = await ParseGuildAsync(id);
        var guildConfig = await guildRepository.GetAsync(guildId);

        if (roleCache.GuildRoles(guildId).All(r => r.ID.Value != roleId))
            throw new ApiError(HttpStatusCode.BadRequest, ErrorCode.BadRequest, "Role not found");

        if (guildConfig.KeyRoles.Contains(roleId))
            throw new ApiError(
                HttpStatusCode.BadRequest,
                ErrorCode.BadRequest,
                "Role is already a key role"
            );

        guildConfig.KeyRoles.Add(roleId);
        await guildRepository.UpdateConfigAsync(guildId, guildConfig);
        return NoContent();
    }

    [HttpDelete("key-roles/{roleId}")]
    public async Task<IActionResult> RemoveKeyRoleAsync(string id, ulong roleId)
    {
        var (guildId, _) = await ParseGuildAsync(id);
        var guildConfig = await guildRepository.GetAsync(guildId);

        if (!guildConfig.KeyRoles.Contains(roleId))
            throw new ApiError(
                HttpStatusCode.BadRequest,
                ErrorCode.BadRequest,
                "Role is already not a key role"
            );

        guildConfig.KeyRoles.Remove(roleId);
        await guildRepository.UpdateConfigAsync(guildId, guildConfig);
        return NoContent();
    }
}
