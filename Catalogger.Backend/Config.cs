// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Serilog.Events;

namespace Catalogger.Backend;

public class Config
{
    public LoggingConfig Logging { get; init; } = new();
    public DatabaseConfig Database { get; init; } = new();
    public DiscordConfig Discord { get; init; } = new();
    public WebConfig Web { get; init; } = new();

    public class LoggingConfig
    {
        public LogEventLevel LogEventLevel { get; init; } = LogEventLevel.Debug;
        public bool LogQueries { get; init; } = false;

        public int MetricsPort { get; init; } = 5001;
        public bool EnableMetrics { get; init; } = true;

        public string? SeqLogUrl { get; init; }
    }

    public class DatabaseConfig
    {
        public string Url { get; init; } = string.Empty;
        public string? Redis { get; init; }
        public int? Timeout { get; init; }
        public int? MaxPoolSize { get; init; }
        public string EncryptionKey { get; init; } = string.Empty;
    }

    public class DiscordConfig
    {
        public ulong ApplicationId { get; set; }
        public string Token { get; init; } = string.Empty;
        public bool SyncCommands { get; init; }
        public ulong? CommandsGuildId { get; init; }
        public ulong? GuildLogId { get; init; }
        public int? ShardCount { get; init; }

        public string ClientSecret { get; init; } = string.Empty;

        public string? SupportGuild { get; init; }
        public bool EnableDash { get; init; } = false;

        // If enabled, nothing will be logged.
        public bool TestMode { get; init; } = false;

        // Token for discord.bots.gg stats
        public string? BotsGgToken { get; init; }
    }

    public class WebConfig
    {
        public string Host { get; init; } = "localhost";
        public int Port { get; init; } = 5000;
        public string BaseUrl { get; init; } = null!;
        public string Address => $"http://{Host}:{Port}";

        public ulong? NewsChannel { get; init; }
    }
}
