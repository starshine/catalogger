// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Collections.Concurrent;
using Catalogger.Backend.Bot;
using Catalogger.Backend.Cache;
using Humanizer;
using Remora.Discord.API.Abstractions.Rest;
using Remora.Discord.API.Gateway.Commands;
using Remora.Rest.Core;

namespace Catalogger.Backend.Services;

public class GuildFetchService(
    ILogger logger,
    ShardedGatewayClient client,
    IDiscordRestGuildAPI guildApi,
    IInviteCache inviteCache
) : BackgroundService
{
    private readonly ILogger _logger = logger.ForContext<GuildFetchService>();
    private readonly ConcurrentQueue<Snowflake> _guilds = new();

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        using var timer = new PeriodicTimer(500.Milliseconds());
        while (await timer.WaitForNextTickAsync(stoppingToken))
        {
            if (!_guilds.TryPeek(out var guildId))
                continue;

            _logger.Debug("Fetching members and invites for guild {GuildId}", guildId);
            client.ClientFor(guildId).SubmitCommand(new RequestGuildMembers(guildId, "", 0));
            var res = await guildApi.GetGuildInvitesAsync(guildId, stoppingToken);
            if (res.Error != null)
            {
                _logger.Error("Fetching invites for guild {GuildId}: {Error}", guildId, res.Error);
            }
            else
            {
                await inviteCache.SetAsync(guildId, res.Entity);
            }

            _guilds.TryDequeue(out _);
        }
    }

    public void EnqueueGuild(Snowflake guildId)
    {
        if (!_guilds.Contains(guildId))
            _guilds.Enqueue(guildId);
    }
}
