// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Catalogger.Backend.Database.Repositories;

namespace Catalogger.Backend.Services;

public class BackgroundTasksService(ILogger logger, IServiceProvider services) : BackgroundService
{
    private readonly ILogger _logger = logger.ForContext<BackgroundTasksService>();

    protected override async Task ExecuteAsync(CancellationToken ct)
    {
        using var timer = new PeriodicTimer(TimeSpan.FromMinutes(1));
        while (await timer.WaitForNextTickAsync(ct))
            await HandlePeriodicTasksAsync(ct);
    }

    private async Task HandlePeriodicTasksAsync(CancellationToken ct = default)
    {
        _logger.Information("Running once per minute periodic tasks");

        await using var scope = services.CreateAsyncScope();
        await using var messageRepository =
            scope.ServiceProvider.GetRequiredService<MessageRepository>();
        await using var timeoutRepository =
            scope.ServiceProvider.GetRequiredService<TimeoutRepository>();

        var (msgCount, ignoredCount) = await messageRepository.DeleteExpiredMessagesAsync();
        if (msgCount != 0 || ignoredCount != 0)
            _logger.Information(
                "Deleted {Count} messages and {IgnoredCount} ignored message IDs older than {MaxDays} days old",
                msgCount,
                ignoredCount,
                MessageRepository.MaxMessageAgeDays
            );

        var timeoutCount = await timeoutRepository.RemoveExpiredTimeoutsAsync();
        if (timeoutCount != 0)
            _logger.Information(
                "Deleted {Count} expired timeouts that were never logged",
                timeoutCount
            );
    }
}
