// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Collections.Concurrent;
using Remora.Discord.API.Abstractions.Objects;
using Remora.Rest.Core;

namespace Catalogger.Backend.Cache.InMemoryCache;

public class InMemoryInviteCache : IInviteCache
{
    private readonly ConcurrentDictionary<Snowflake, IEnumerable<IInviteWithMetadata>> _invites =
        new();

    public Task<IEnumerable<IInviteWithMetadata>> TryGetAsync(Snowflake guildId) =>
        _invites.TryGetValue(guildId, out var invites)
            ? Task.FromResult(invites)
            : Task.FromResult<IEnumerable<IInviteWithMetadata>>([]);

    public Task SetAsync(Snowflake guildId, IEnumerable<IInviteWithMetadata> invites)
    {
        _invites[guildId] = invites;
        return Task.CompletedTask;
    }

    public Task RemoveAsync(Snowflake guildId)
    {
        _invites.Remove(guildId, out _);
        return Task.CompletedTask;
    }
}
