// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Collections.Concurrent;
using System.Diagnostics.CodeAnalysis;
using Remora.Discord.API;
using Remora.Discord.API.Abstractions.Objects;
using Remora.Rest.Core;

namespace Catalogger.Backend.Cache.InMemoryCache;

public class GuildCache
{
    private readonly ConcurrentDictionary<Snowflake, IGuild> _guilds = new();

    public int Size => _guilds.Count;

    public bool Contains(Snowflake id) => _guilds.ContainsKey(id);

    public bool Contains(string id) =>
        DiscordSnowflake.TryParse(id, out var sf) && _guilds.ContainsKey(sf.Value);

    public void Set(IGuild guild) => _guilds[guild.ID] = guild;

    public bool Remove(Snowflake id, [NotNullWhen(true)] out IGuild? guild) =>
        _guilds.Remove(id, out guild);

    public bool TryGet(Snowflake id, [NotNullWhen(true)] out IGuild? guild) =>
        _guilds.TryGetValue(id, out guild);
}
