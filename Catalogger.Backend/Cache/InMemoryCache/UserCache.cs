// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Catalogger.Backend.Extensions;
using LazyCache;
using Remora.Discord.API.Abstractions.Objects;
using Remora.Discord.API.Abstractions.Rest;
using Remora.Rest.Core;

namespace Catalogger.Backend.Cache.InMemoryCache;

public class UserCache(IDiscordRestUserAPI userApi)
{
    private readonly IAppCache _cache = new CachingService();
    private int _cacheSize = 0;

    public int Size => _cacheSize;

    public async Task<IUser?> GetUserAsync(Snowflake userId) =>
        await _cache.GetOrAddAsync(
            userId.ToString(),
            async () =>
            {
                var user = await userApi.GetUserAsync(userId).GetOrThrow();
                Interlocked.Increment(ref _cacheSize);
                return user;
            }
        );

    public void UpdateUser(IUser user) => _cache.Add(user.ID.ToString(), user);
}
