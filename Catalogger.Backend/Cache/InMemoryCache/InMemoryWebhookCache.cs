// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Collections.Concurrent;

namespace Catalogger.Backend.Cache.InMemoryCache;

public class InMemoryWebhookCache : IWebhookCache
{
    private readonly ConcurrentDictionary<ulong, Webhook> _cache = new();

    public Task<Webhook?> GetWebhookAsync(ulong channelId)
    {
        return _cache.TryGetValue(channelId, out var webhook)
            ? Task.FromResult<Webhook?>(webhook)
            : Task.FromResult<Webhook?>(null);
    }

    public Task SetWebhookAsync(ulong channelId, Webhook webhook)
    {
        _cache[channelId] = webhook;
        return Task.CompletedTask;
    }

    public Task RemoveWebhooksAsync(ulong[] channelIds)
    {
        foreach (var id in channelIds)
            _cache.TryRemove(id, out _);
        return Task.CompletedTask;
    }
}
