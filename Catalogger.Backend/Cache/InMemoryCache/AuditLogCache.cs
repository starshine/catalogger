// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Collections.Concurrent;
using Remora.Discord.API;
using Remora.Rest.Core;

namespace Catalogger.Backend.Cache.InMemoryCache;

public class AuditLogCache
{
    private readonly ConcurrentDictionary<
        (Snowflake GuildId, Snowflake TargetId),
        ActionData
    > _kicks = new();

    private readonly ConcurrentDictionary<
        (Snowflake GuildId, Snowflake TargetId),
        ActionData
    > _bans = new();

    private readonly ConcurrentDictionary<
        (Snowflake GuildId, Snowflake TargetId),
        ActionData
    > _unbans = new();

    private readonly ConcurrentDictionary<
        (Snowflake GuildId, Snowflake TargetId),
        ActionData
    > _memberUpdates = new();

    public void SetKick(
        Snowflake guildId,
        string targetId,
        Snowflake moderatorId,
        Optional<string> reason
    )
    {
        if (!DiscordSnowflake.TryParse(targetId, out var targetUser))
            throw new CataloggerError("Target ID was not a valid snowflake");

        _kicks[(guildId, targetUser.Value)] = new ActionData(moderatorId, reason.OrDefault());
    }

    public bool TryGetKick(Snowflake guildId, Snowflake targetId, out ActionData data) =>
        _kicks.TryGetValue((guildId, targetId), out data);

    public void SetBan(
        Snowflake guildId,
        string targetId,
        Snowflake moderatorId,
        Optional<string> reason
    )
    {
        if (!DiscordSnowflake.TryParse(targetId, out var targetUser))
            throw new CataloggerError("Target ID was not a valid snowflake");

        _bans[(guildId, targetUser.Value)] = new ActionData(moderatorId, reason.OrDefault());
    }

    public bool TryGetBan(Snowflake guildId, Snowflake targetId, out ActionData data) =>
        _bans.TryGetValue((guildId, targetId), out data);

    public void SetUnban(
        Snowflake guildId,
        string targetId,
        Snowflake moderatorId,
        Optional<string> reason
    )
    {
        if (!DiscordSnowflake.TryParse(targetId, out var targetUser))
            throw new CataloggerError("Target ID was not a valid snowflake");

        _unbans[(guildId, targetUser.Value)] = new ActionData(moderatorId, reason.OrDefault());
    }

    public bool TryGetUnban(Snowflake guildId, Snowflake targetId, out ActionData data) =>
        _unbans.TryGetValue((guildId, targetId), out data);

    public void SetMemberUpdate(
        Snowflake guildId,
        string targetId,
        Snowflake moderatorId,
        Optional<string> reason
    )
    {
        if (!DiscordSnowflake.TryParse(targetId, out var targetUser))
            throw new CataloggerError("Target ID was not a valid snowflake");

        _memberUpdates[(guildId, targetUser.Value)] = new ActionData(
            moderatorId,
            reason.OrDefault()
        );
    }

    public bool TryGetMemberUpdate(Snowflake guildId, Snowflake targetId, out ActionData data) =>
        _memberUpdates.TryGetValue((guildId, targetId), out data);

    public record struct ActionData(Snowflake ModeratorId, string? Reason);
}
