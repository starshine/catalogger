// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Collections.Concurrent;
using System.Diagnostics.CodeAnalysis;
using Remora.Discord.API.Abstractions.Objects;
using Remora.Rest.Core;

namespace Catalogger.Backend.Cache.InMemoryCache;

public class EmojiCache
{
    private readonly ConcurrentDictionary<Snowflake, IReadOnlyList<IEmoji>> _emojis = new();

    public void Set(Snowflake guildId, IReadOnlyList<IEmoji> emoji) => _emojis[guildId] = emoji;

    public bool TryGet(Snowflake guildId, [NotNullWhen(true)] out IReadOnlyList<IEmoji>? emoji) =>
        _emojis.TryGetValue(guildId, out emoji);

    public int Size => _emojis.Select(kv => kv.Value.Count).Sum();

    public bool Remove(Snowflake guildId) => _emojis.Remove(guildId, out _);
}
