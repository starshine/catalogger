// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Catalogger.Backend.Extensions;
using Remora.Discord.API;
using Remora.Discord.API.Abstractions.Objects;
using Remora.Rest.Core;

namespace Catalogger.Backend.Cache;

public interface IWebhookCache
{
    Task<Webhook?> GetWebhookAsync(ulong channelId);
    Task SetWebhookAsync(ulong channelId, Webhook webhook);
    Task RemoveWebhooksAsync(ulong[] channelIds);

    public async Task<Webhook> GetOrFetchWebhookAsync(
        ulong channelId,
        Func<Snowflake, Task<IWebhook>> fetch
    )
    {
        var webhook = await GetWebhookAsync(channelId);
        if (webhook != null)
            return webhook.Value;

        var discordWebhook = await fetch(DiscordSnowflake.New(channelId));
        webhook = new Webhook
        {
            Id = discordWebhook.ID.ToUlong(),
            Token = discordWebhook.Token.Value,
        };
        await SetWebhookAsync(channelId, webhook.Value);
        return webhook.Value;
    }
}

public struct Webhook
{
    public required ulong Id { get; init; }
    public required string Token { get; init; }
}
