// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Catalogger.Backend.Database.Redis;
using Humanizer;

namespace Catalogger.Backend.Cache.RedisCache;

public class RedisWebhookCache(RedisService redisService) : IWebhookCache
{
    public async Task<Webhook?> GetWebhookAsync(ulong channelId) =>
        await redisService.GetAsync<Webhook?>(WebhookKey(channelId));

    public async Task SetWebhookAsync(ulong channelId, Webhook webhook) =>
        await redisService.SetAsync(WebhookKey(channelId), webhook, 24.Hours());

    public async Task RemoveWebhooksAsync(ulong[] channelIds) =>
        await redisService.DeleteAsync(channelIds.Select(WebhookKey).ToArray());

    private static string WebhookKey(ulong channelId) => $"webhook:{channelId}";
}
