// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Remora.Discord.API.Abstractions.Gateway.Events;
using Remora.Discord.API.Abstractions.Objects;
using Remora.Rest.Core;

namespace Catalogger.Backend.Cache;

public interface IMemberCache
{
    public Task<IGuildMember?> TryGetAsync(Snowflake guildId, Snowflake userId);
    public Task SetAsync(Snowflake guildId, IGuildMember member);
    public Task SetManyAsync(Snowflake guildId, IReadOnlyList<IGuildMember> members);
    public Task RemoveAsync(Snowflake guildId, Snowflake userId);
    public Task<bool> IsGuildCachedAsync(Snowflake guildId);
    public Task MarkAsCachedAsync(Snowflake guildId);
    public Task MarkAsUncachedAsync(Snowflake guildId);
    public Task UpdateAsync(IGuildMemberUpdate newMember);

    // These methods can be stubbed out for any implementation that isn't intended for use with the dashboard.
    public Task SetMemberNamesAsync(Snowflake guildId, IEnumerable<IGuildMember> members);

    public Task UpdateMemberNameAsync(
        Snowflake guildId,
        Snowflake userId,
        string prevName,
        string newName
    );

    public Task<IEnumerable<(string Name, string Id)>> GetMemberNamesAsync(
        Snowflake guildId,
        string prefix
    );

    public Task TryRemoveMemberNameAsync(Snowflake guildId, string username);

    public Task RemoveAllMembersAsync(Snowflake guildId);
}
