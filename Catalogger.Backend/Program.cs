// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Text.Json;
using System.Text.Json.Serialization;
using Catalogger.Backend;
using Catalogger.Backend.Bot.Commands;
using Catalogger.Backend.Extensions;
using Catalogger.Backend.Services;
using NodaTime.Serialization.SystemTextJson;
using Prometheus;
using Remora.Commands.Extensions;
using Remora.Discord.API.Abstractions.Gateway.Commands;
using Remora.Discord.API.Abstractions.Objects;
using Remora.Discord.API.Gateway.Commands;
using Remora.Discord.API.Objects;
using Remora.Discord.Commands.Extensions;
using Remora.Discord.Commands.Responders;
using Remora.Discord.Extensions.Extensions;
using Remora.Discord.Gateway;
using Remora.Discord.Interactivity.Extensions;
using Remora.Discord.Pagination.Extensions;
using Serilog;
using Metrics = Prometheus.Metrics;

var builder = WebApplication.CreateBuilder(args);
var config = builder.AddConfiguration();
builder.AddSerilog(config);

builder
    .Services.AddControllers()
    .AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.SnakeCaseLower;
        options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
        options.JsonSerializerOptions.IncludeFields = true;
        options.JsonSerializerOptions.NumberHandling =
            JsonNumberHandling.WriteAsString | JsonNumberHandling.AllowReadingFromString;
        options.JsonSerializerOptions.ConfigureForNodaTime(JsonUtils.NodaTimeSettings);
    });

builder
    .Host.AddShardedDiscordService(_ => config.Discord.Token)
    .ConfigureServices(s =>
        s.AddRespondersFromAssembly(typeof(Program).Assembly)
            .Configure<DiscordGatewayClientOptions>(g =>
            {
                g.Intents =
                    GatewayIntents.Guilds
                    // Actually GUILD_MODERATION
                    | GatewayIntents.GuildBans
                    | GatewayIntents.GuildInvites
                    | GatewayIntents.GuildMembers
                    | GatewayIntents.GuildMessages
                    | GatewayIntents.GuildWebhooks
                    | GatewayIntents.MessageContents
                    // Actually GUILD_EXPRESSIONS
                    | GatewayIntents.GuildEmojisAndStickers;

                // Set a default status for all shards. This is updated to a shard-specific one in StatusUpdateService.
                g.Presence = new UpdatePresence(
                    Status: UserStatus.Online,
                    IsAFK: false,
                    Since: null,
                    Activities:
                    [
                        new Activity(
                            Name: "Beep",
                            Type: ActivityType.Custom,
                            State: "/catalogger help"
                        ),
                    ]
                );
            })
            .Configure<InteractionResponderOptions>(opts => opts.SuppressAutomaticResponses = true)
            .AddDiscordCommands(
                enableSlash: true,
                useDefaultCommandResponder: false,
                useDefaultInteractionResponder: false
            )
            .AddCommandTree()
            // Start command tree
            .WithCommandGroup<MetaCommands>()
            .WithCommandGroup<ChannelCommands>()
            .WithCommandGroup<KeyRoleCommands>()
            .WithCommandGroup<InviteCommands>()
            .WithCommandGroup<IgnoreMessageCommands>()
            .WithCommandGroup<IgnoreEntitiesCommands>()
            .WithCommandGroup<RedirectCommands>()
            .WithCommandGroup<WatchlistCommands>()
            // End command tree
            .Finish()
            .AddPagination()
            .AddInteractivity()
            .AddInteractionGroup<ChannelCommandsComponents>()
            .AddAutocompleteProvider<InviteAutocompleteProvider>()
    );

// Add metric server
// If metrics are disabled (Logging.EnableMetrics = false), also add a background service that updates
// metrics every minute, as some commands rely on them.
builder.Services.AddMetricServer(o => o.Port = (ushort)config.Logging.MetricsPort);
if (!config.Logging.EnableMetrics)
    builder.Services.AddHostedService<BackgroundMetricsCollectionService>();

builder.Services.MaybeAddDashboardServices(config).MaybeAddRedisCaches(config).AddCustomServices();

var app = builder.Build();

await app.Initialize();
app.MaybeAddDashboard();

app.Urls.Clear();
app.Urls.Add(config.Web.Address);

// Make sure metrics are updated whenever Prometheus scrapes them
Metrics.DefaultRegistry.AddBeforeCollectCallback(async ct =>
    await app.Services.GetRequiredService<MetricsCollectionService>().CollectMetricsAsync(ct)
);

app.Run();
Log.CloseAndFlush();
