using System.Text.Json;
using System.Text.Json.Serialization;
using NodaTime;
using NodaTime.Serialization.SystemTextJson;
using NodaTime.Text;

namespace Catalogger.Backend;

public static class JsonUtils
{
    public static readonly NodaJsonSettings NodaTimeSettings = new NodaJsonSettings
    {
        InstantConverter = new NodaPatternConverter<Instant>(InstantPattern.ExtendedIso),
    };

    public static readonly JsonSerializerOptions BaseJsonOptions = new JsonSerializerOptions
    {
        NumberHandling = JsonNumberHandling.AllowReadingFromString,
    }.ConfigureForNodaTime(NodaTimeSettings);

    public static readonly JsonSerializerOptions ApiJsonOptions = new JsonSerializerOptions
    {
        NumberHandling =
            JsonNumberHandling.WriteAsString | JsonNumberHandling.AllowReadingFromString,
        PropertyNamingPolicy = JsonNamingPolicy.SnakeCaseLower,
    }.ConfigureForNodaTime(NodaTimeSettings);
}
