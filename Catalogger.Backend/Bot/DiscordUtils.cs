// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using System.Drawing;
using Remora.Discord.API;
using Remora.Discord.API.Abstractions.Objects;
using Remora.Discord.API.Objects;
using Remora.Discord.Pagination;
using Remora.Rest.Core;

namespace Catalogger.Backend.Bot;

public static class DiscordUtils
{
    public static readonly Snowflake PkUserId = DiscordSnowflake.New(466378653216014359);

    public static readonly Color Red = Color.FromArgb(231, 76, 60);
    public static readonly Color Purple = Color.FromArgb(155, 89, 182);
    public static readonly Color Green = Color.FromArgb(46, 204, 113);
    public static readonly Color Blue = Color.FromArgb(52, 152, 219);
    public static readonly Color Orange = Color.FromArgb(230, 126, 34);

    public static List<Embed> PaginateFields(
        IEnumerable<IEmbedField> fields,
        Optional<string> title = default,
        string description = "",
        uint fieldsPerPage = 6
    ) =>
        PageFactory.FromFields(
            fields,
            fieldsPerPage,
            description,
            new Embed(Title: title, Colour: Purple)
        );

    public static List<Embed> PaginateStrings(
        IEnumerable<string> strings,
        Optional<string> title = default,
        int stringsPerPage = 20
    )
    {
        var pages = strings.ToArray().Split(stringsPerPage);
        return pages
            .Select(p => new Embed(
                Title: title,
                Colour: Purple,
                Description: string.Join("\n", p.Select((row, i) => $"{i + 1}. {row}"))
            ))
            .ToList();
    }

    private static IEnumerable<IEnumerable<T>> Split<T>(this T[] arr, int size)
    {
        for (var i = 0; i < arr.Length / size + 1; i++)
        {
            yield return arr.Skip(i * size).Take(size);
        }
    }
}
