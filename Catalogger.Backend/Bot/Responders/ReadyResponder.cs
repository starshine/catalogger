// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Catalogger.Backend.Extensions;
using Catalogger.Backend.Services;
using Remora.Discord.API.Abstractions.Gateway.Events;
using Remora.Discord.Gateway.Responders;
using Remora.Results;

namespace Catalogger.Backend.Bot.Responders;

public class ReadyResponder(ILogger logger, WebhookExecutorService webhookExecutorService)
    : IResponder<IReady>
{
    private readonly ILogger _logger = logger.ForContext<ReadyResponder>();

    public Task<Result> RespondAsync(IReady evt, CancellationToken ct = default)
    {
        using var _ = LogUtils.Enrich(evt);

        var shardId = evt.Shard.TryGet(out var shard) ? (shard.ShardID, shard.ShardCount) : (0, 1);
        _logger.Information(
            "Ready as {User} on shard {ShardId}/{ShardCount}",
            evt.User.Tag(),
            shardId.Item1,
            shardId.Item2
        );
        if (shardId.Item1 == 0)
            webhookExecutorService.SetSelfUser(evt.User);

        return Task.FromResult(Result.Success);
    }
}
