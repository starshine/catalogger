// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Catalogger.Backend.Cache.InMemoryCache;
using Catalogger.Backend.Database.Repositories;
using Catalogger.Backend.Extensions;
using Catalogger.Backend.Services;
using Remora.Discord.API.Abstractions.Gateway.Events;
using Remora.Discord.Extensions.Embeds;
using Remora.Discord.Gateway.Responders;
using Remora.Results;

namespace Catalogger.Backend.Bot.Responders.Roles;

public class RoleCreateResponder(
    ILogger logger,
    GuildRepository guildRepository,
    RoleCache roleCache,
    WebhookExecutorService webhookExecutor
) : IResponder<IGuildRoleCreate>
{
    private readonly ILogger _logger = logger.ForContext<RoleCreateResponder>();

    public async Task<Result> RespondAsync(IGuildRoleCreate evt, CancellationToken ct = default)
    {
        using var _ = LogUtils.Enrich(evt);

        _logger.Debug("Received new role {RoleId} in guild {GuildId}", evt.Role.ID, evt.GuildID);
        roleCache.Set(evt.Role, evt.GuildID);

        var guildConfig = await guildRepository.GetAsync(evt.GuildID);

        var embed = new EmbedBuilder()
            .WithTitle("Role created")
            .WithColour(DiscordUtils.Green)
            .WithDescription(
                $"**Name:** {evt.Role.Name}\n**Colour:** {evt.Role.Colour.ToPrettyString()}"
                    + $"\n**Mentionable:** {evt.Role.IsMentionable}\n**Shown separately:** {evt.Role.IsHoisted}"
            );

        if (!evt.Role.Permissions.Value.IsZero)
        {
            embed.AddField("Permissions", evt.Role.Permissions.ToPrettyString(), inline: false);
        }

        webhookExecutor.QueueLog(
            webhookExecutor.GetLogChannel(
                guildConfig,
                LogChannelType.GuildRoleCreate,
                roleId: evt.Role.ID
            ),
            embed.Build().GetOrThrow()
        );

        return Result.Success;
    }
}
