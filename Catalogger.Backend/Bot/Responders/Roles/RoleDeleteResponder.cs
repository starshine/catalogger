// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Catalogger.Backend.Cache.InMemoryCache;
using Catalogger.Backend.Database.Repositories;
using Catalogger.Backend.Extensions;
using Catalogger.Backend.Services;
using Remora.Discord.API.Abstractions.Gateway.Events;
using Remora.Discord.Extensions.Embeds;
using Remora.Discord.Gateway.Responders;
using Remora.Results;

namespace Catalogger.Backend.Bot.Responders.Roles;

public class RoleDeleteResponder(
    ILogger logger,
    GuildRepository guildRepository,
    RoleCache roleCache,
    WebhookExecutorService webhookExecutor
) : IResponder<IGuildRoleDelete>
{
    private readonly ILogger _logger = logger.ForContext<RoleDeleteResponder>();

    public async Task<Result> RespondAsync(IGuildRoleDelete evt, CancellationToken ct = default)
    {
        using var __ = LogUtils.Enrich(evt);

        try
        {
            if (!roleCache.TryGet(evt.RoleID, out var role))
            {
                _logger.Information(
                    "Received role delete event for {RoleId} but it wasn't cached, ignoring",
                    evt.RoleID
                );
                return Result.Success;
            }

            var guildConfig = await guildRepository.GetAsync(evt.GuildID);

            var embed = new EmbedBuilder()
                .WithTitle($"Role \"{role.Name}\" deleted")
                .WithColour(DiscordUtils.Red)
                .WithFooter($"ID: {role.ID}")
                .WithCurrentTimestamp()
                .WithDescription(
                    $"""
                    **Name:** {role.Name}
                    **Colour:** {role.Colour.ToPrettyString()}
                    **Mentionable:** {role.IsMentionable}
                    **Shown separately:** {role.IsHoisted}
                    **Position:** {role.Position}
                    Created <t:{role.ID.Timestamp.ToUnixTimeSeconds()}> ({role.ID.Timestamp.Prettify()} ago)
                    """
                );

            if (!role.Permissions.Value.IsZero)
            {
                embed.AddField("Permissions", role.Permissions.ToPrettyString());
            }

            webhookExecutor.QueueLog(
                webhookExecutor.GetLogChannel(
                    guildConfig,
                    LogChannelType.GuildRoleDelete,
                    roleId: role.ID
                ),
                embed.Build().GetOrThrow()
            );
        }
        finally
        {
            roleCache.Remove(evt.RoleID, evt.GuildID, out _);
        }

        return Result.Success;
    }
}
