// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Catalogger.Backend.Cache.InMemoryCache;
using Catalogger.Backend.Database.Repositories;
using Catalogger.Backend.Extensions;
using Catalogger.Backend.Services;
using Remora.Discord.API.Abstractions.Gateway.Events;
using Remora.Discord.Extensions.Embeds;
using Remora.Discord.Gateway.Responders;
using Remora.Results;

namespace Catalogger.Backend.Bot.Responders.Guilds;

public class GuildBanRemoveResponder(
    ILogger logger,
    GuildRepository guildRepository,
    WebhookExecutorService webhookExecutor,
    UserCache userCache,
    AuditLogCache auditLogCache,
    PluralkitApiService pluralkitApi
) : IResponder<IGuildBanRemove>
{
    private readonly ILogger _logger = logger.ForContext<GuildBanRemoveResponder>();

    public async Task<Result> RespondAsync(IGuildBanRemove evt, CancellationToken ct = default)
    {
        using var _ = LogUtils.Enrich(evt);
        var guildConfig = await guildRepository.GetAsync(evt.GuildID);

        // Delay 2 seconds for the audit log
        await Task.Delay(2000, ct);

        var embed = new EmbedBuilder()
            .WithTitle("User unbanned")
            .WithAuthor(evt.User.Tag(), null, evt.User.AvatarUrl())
            .WithDescription($"<@{evt.User.ID}>")
            .WithColour(DiscordUtils.Green)
            .WithFooter($"User ID: {evt.User.ID}")
            .WithCurrentTimestamp();

        if (auditLogCache.TryGetUnban(evt.GuildID, evt.User.ID, out var actionData))
        {
            embed.AddField(
                "Responsible moderator",
                await userCache.TryFormatUserAsync(actionData.ModeratorId)
            );
            embed.AddField("Reason", actionData.Reason ?? "No reason given");
        }
        else
        {
            embed.AddField("Responsible moderator", "*(unknown)*");
            embed.AddField("Reason", "*(unknown)*");
        }

        var pkSystem = await pluralkitApi.GetPluralKitSystemAsync(evt.User.ID.Value, ct);
        if (pkSystem != null)
        {
            await guildRepository.UnbanSystemAsync(
                evt.GuildID,
                evt.User.ID,
                pkSystem.Id,
                pkSystem.Uuid
            );

            var systemUsers = await guildRepository.GetSystemAccountsAsync(
                evt.GuildID,
                pkSystem.Uuid
            );
            if (systemUsers.Length == 0)
            {
                embed.AddField(
                    "PluralKit system",
                    $"""
                    **ID:** {pkSystem.Id}
                    **UUID:** `{pkSystem.Uuid}`
                    **Name:** {pkSystem.Name ?? "*(none)*"}
                    **Tag:** {pkSystem.Tag ?? "*(none)*"}

                    This system has been unbanned.
                    Note that other accounts linked to the system might still be banned, check `pk;system {pkSystem.Id}` for the linked accounts.
                    """
                );
            }
            else
            {
                var users = new List<string>();
                foreach (var id in systemUsers)
                    users.Add("- " + await userCache.TryFormatUserAsync(id));

                embed.AddField(
                    "PluralKit system",
                    $"""
                    **ID:** {pkSystem.Id}
                    **UUID:** `{pkSystem.Uuid}`
                    **Name:** {pkSystem.Name ?? "*(none)*"}
                    **Tag:** {pkSystem.Tag ?? "*(none)*"}

                    This system has been unbanned.
                    Note that the following accounts are known to be linked to this system and banned from this server:
                    {string.Join("\n", users)}
                    """
                );
            }
        }

        webhookExecutor.QueueLog(
            guildConfig,
            LogChannelType.GuildBanRemove,
            embed.Build().GetOrThrow()
        );
        return Result.Success;
    }
}
