// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Remora.Discord.Gateway.Results;

namespace Catalogger.Backend.Bot;

public class ShardedDiscordService(
    ILogger logger,
    ShardedGatewayClient client,
    IHostApplicationLifetime lifetime
) : BackgroundService
{
    private readonly ILogger _logger = logger.ForContext<ShardedDiscordService>();

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            _logger.Information("Starting sharded Discord client");
            var result = await client.RunAsync(stoppingToken);
            _logger.Information("Discord client finished running");
            if (result.Error is GatewayError { IsCritical: true })
            {
                lifetime.StopApplication();
                break;
            }
        }
    }
}
