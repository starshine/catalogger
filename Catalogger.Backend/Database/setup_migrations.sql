create table if not exists migrations (
    migration_name text primary key,
    applied_at     timestamptz not null default (now())
);
