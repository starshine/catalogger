create table pluralkit_systems (
    system_id uuid not null,
    user_id bigint not null,
    guild_id bigint not null,
    
    primary key (system_id, user_id, guild_id)
);

create index ix_pluralkit_systems_user_guild on pluralkit_systems (user_id, guild_id);
