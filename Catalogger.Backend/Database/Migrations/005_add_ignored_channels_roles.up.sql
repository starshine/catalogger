alter table guilds add column ignored_channels bigint[] not null default array[]::bigint[];
alter table guilds add column ignored_roles bigint[] not null default array[]::bigint[];
