alter table guilds
    add column messages jsonb not null default '{}';

-- Extract the current message-related configuration options into the new "messages" column
-- noinspection SqlWithoutWhere
update guilds
set messages = jsonb_build_object('IgnoredUsers', channels['IgnoredUsers'], 'IgnoredChannels',
                                  channels['IgnoredChannels'], 'IgnoredUsersPerChannel',
                                  channels['IgnoredUsersPerChannel']);

-- We don't update the "channels" column as it will be cleared out automatically over time,
-- as channel configurations are updated by the bot
