update guilds set channels = (channels || messages) - 'IgnoredRoles';

alter table guilds drop column messages;
