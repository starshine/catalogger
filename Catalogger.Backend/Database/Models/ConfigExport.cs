using NodaTime;

namespace Catalogger.Backend.Database.Models;

public record ConfigExport(
    ulong Id,
    ChannelsBackup Channels,
    string[] BannedSystems,
    List<ulong> KeyRoles,
    IEnumerable<InviteExport> Invites,
    IEnumerable<WatchlistExport> Watchlist
);

public record InviteExport(string Code, string Name);

public record WatchlistExport(ulong UserId, Instant AddedAt, ulong ModeratorId, string Reason);

public class ChannelsBackup
{
    public List<ulong> IgnoredChannels { get; init; } = [];
    public List<ulong> IgnoredUsers { get; init; } = [];
    public List<ulong> IgnoredRoles { get; init; } = [];
    public Dictionary<ulong, List<ulong>> IgnoredUsersPerChannel { get; init; } = [];
    public Dictionary<ulong, ulong> Redirects { get; init; } = [];

    public ulong GuildUpdate { get; init; }
    public ulong GuildEmojisUpdate { get; init; }
    public ulong GuildRoleCreate { get; init; }
    public ulong GuildRoleUpdate { get; init; }
    public ulong GuildRoleDelete { get; init; }
    public ulong ChannelCreate { get; init; }
    public ulong ChannelUpdate { get; init; }
    public ulong ChannelDelete { get; init; }
    public ulong GuildMemberAdd { get; init; }
    public ulong GuildMemberUpdate { get; init; }
    public ulong GuildKeyRoleUpdate { get; init; }
    public ulong GuildMemberNickUpdate { get; init; }
    public ulong GuildMemberAvatarUpdate { get; init; }
    public ulong GuildMemberTimeout { get; init; }
    public ulong GuildMemberRemove { get; init; }
    public ulong GuildMemberKick { get; init; }
    public ulong GuildBanAdd { get; init; }
    public ulong GuildBanRemove { get; init; }
    public ulong InviteCreate { get; init; }
    public ulong InviteDelete { get; init; }
    public ulong MessageUpdate { get; init; }
    public ulong MessageDelete { get; init; }
    public ulong MessageDeleteBulk { get; init; }

    public Guild.MessageConfig ToMessageConfig() =>
        new()
        {
            IgnoredChannels = IgnoredChannels,
            IgnoredUsers = IgnoredUsers,
            IgnoredRoles = IgnoredRoles,
            IgnoredUsersPerChannel = IgnoredUsersPerChannel,
        };

    public Guild.ChannelConfig ToChannelConfig() =>
        new()
        {
            Redirects = Redirects,
            GuildUpdate = GuildUpdate,
            GuildEmojisUpdate = GuildEmojisUpdate,
            GuildRoleCreate = GuildRoleCreate,
            GuildRoleUpdate = GuildRoleUpdate,
            GuildRoleDelete = GuildRoleDelete,
            ChannelCreate = ChannelCreate,
            ChannelUpdate = ChannelUpdate,
            ChannelDelete = ChannelDelete,
            GuildMemberAdd = GuildMemberAdd,
            GuildMemberUpdate = GuildMemberUpdate,
            GuildKeyRoleUpdate = GuildKeyRoleUpdate,
            GuildMemberNickUpdate = GuildMemberNickUpdate,
            GuildMemberAvatarUpdate = GuildMemberAvatarUpdate,
            GuildMemberTimeout = GuildMemberTimeout,
            GuildMemberRemove = GuildMemberRemove,
            GuildMemberKick = GuildMemberKick,
            GuildBanAdd = GuildBanAdd,
            GuildBanRemove = GuildBanRemove,
            InviteCreate = InviteCreate,
            InviteDelete = InviteDelete,
            MessageUpdate = MessageUpdate,
            MessageDelete = MessageDelete,
            MessageDeleteBulk = MessageDeleteBulk,
        };

    public static ChannelsBackup FromGuildConfig(Guild guild) =>
        new()
        {
            IgnoredChannels = guild.Messages.IgnoredChannels,
            IgnoredUsers = guild.Messages.IgnoredUsers,
            IgnoredRoles = guild.Messages.IgnoredRoles,
            IgnoredUsersPerChannel = guild.Messages.IgnoredUsersPerChannel,
            Redirects = guild.Channels.Redirects,
            GuildUpdate = guild.Channels.GuildUpdate,
            GuildEmojisUpdate = guild.Channels.GuildEmojisUpdate,
            GuildRoleCreate = guild.Channels.GuildRoleCreate,
            GuildRoleUpdate = guild.Channels.GuildRoleUpdate,
            GuildRoleDelete = guild.Channels.GuildRoleDelete,
            ChannelCreate = guild.Channels.ChannelCreate,
            ChannelUpdate = guild.Channels.ChannelUpdate,
            ChannelDelete = guild.Channels.ChannelDelete,
            GuildMemberAdd = guild.Channels.GuildMemberAdd,
            GuildMemberUpdate = guild.Channels.GuildMemberUpdate,
            GuildKeyRoleUpdate = guild.Channels.GuildKeyRoleUpdate,
            GuildMemberNickUpdate = guild.Channels.GuildMemberNickUpdate,
            GuildMemberAvatarUpdate = guild.Channels.GuildMemberAvatarUpdate,
            GuildMemberTimeout = guild.Channels.GuildMemberTimeout,
            GuildMemberRemove = guild.Channels.GuildMemberRemove,
            GuildMemberKick = guild.Channels.GuildMemberKick,
            GuildBanAdd = guild.Channels.GuildBanAdd,
            GuildBanRemove = guild.Channels.GuildBanRemove,
            InviteCreate = guild.Channels.InviteCreate,
            InviteDelete = guild.Channels.InviteDelete,
            MessageUpdate = guild.Channels.MessageUpdate,
            MessageDelete = guild.Channels.MessageDelete,
            MessageDeleteBulk = guild.Channels.MessageDeleteBulk,
        };
}
