using NodaTime;

namespace Catalogger.Backend.Database.Models;

public class DiscordTimeout
{
    public int Id { get; init; }
    public ulong UserId { get; init; }
    public ulong GuildId { get; init; }
    public ulong? ModeratorId { get; init; }
    public Instant Until { get; init; }
}
