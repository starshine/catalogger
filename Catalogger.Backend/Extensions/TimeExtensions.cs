// Copyright (C) 2021-present sam (starshines.gay)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

using Humanizer;
using Humanizer.Localisation;
using NodaTime;

namespace Catalogger.Backend.Extensions;

public static class TimeExtensions
{
    public static string Prettify(this TimeSpan timespan, TimeUnit minUnit = TimeUnit.Minute) =>
        timespan.Humanize(
            maxUnit: TimeUnit.Year,
            minUnit: minUnit,
            precision: 5,
            collectionSeparator: null
        );

    public static string Prettify(this Duration duration, TimeUnit minUnit = TimeUnit.Minute) =>
        duration.ToTimeSpan().Prettify(minUnit);

    public static string Prettify(
        this DateTimeOffset datetime,
        TimeUnit minUnit = TimeUnit.Minute
    ) => (datetime - DateTimeOffset.Now).Prettify(minUnit);

    public static string Prettify(this Instant instant, TimeUnit minUnit = TimeUnit.Minute) =>
        (instant - SystemClock.Instance.GetCurrentInstant()).Prettify(minUnit);
}
