package main

import (
	"context"
	"log"
	"os"
	"strconv"

	"github.com/jackc/pgx/v4/pgxpool"
)

var conn *pgxpool.Pool
var ctx = context.Background()
var aesKey [32]byte

func main() {
	copy(aesKey[:], []byte(os.Getenv("AES_KEY")))
	dsn := os.Getenv("DATABASE_URL")

	pool, err := pgxpool.Connect(ctx, dsn)
	if err != nil {
		log.Fatalf("error connecting to db: %v\n", err)
	}
	conn = pool

	err = exportGuilds()
	if err != nil {
		log.Fatalf("error exporting guilds: %v\n", err)
	}

	if ok, _ := strconv.ParseBool(os.Getenv("EXPORT_MESSAGES")); ok {
		err = exportMessages()
		if err != nil {
			log.Fatalf("error exporting messages: %v\n", err)
		}
	}
}
