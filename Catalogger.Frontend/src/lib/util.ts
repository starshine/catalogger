import type { FullGuild } from "./api";

export const makeFullOptions = (guild: FullGuild, ignore: string[]) => {
	const options = [];

	options.push(
		...guild.categories
			.filter((cat) => !ignore.some((k) => k === cat.id))
			.map((cat) => ({
				label: `${cat.name} (category)`,
				value: cat.id,
			})),
	);

	// Filter these channels
	const channelsWithoutCategory = guild.channels_without_category.filter(
		(c) => c.can_redirect_from && !ignore.some((k) => k === c.id),
	);
	if (channelsWithoutCategory.length > 0)
		options.push({
			label: "(no category)",
			options: channelsWithoutCategory.map((c) => ({
				value: c.id,
				label: `#${c.name}`,
			})),
		});

	options.push(
		...guild.categories
			.map((cat) => ({
				label: cat.name,
				options: cat.channels
					.filter((c) => c.can_redirect_from && !ignore.some((k) => k === c.id))
					.map((c) => ({ value: c.id, label: `#${c.name}` })),
			}))
			.filter((c) => c.options.length > 0),
	);

	return options;
};

export const makeFullOptions2 = (guild: FullGuild, ignore: string[]) => {
	const options: Array<{ label: string; value: string; group: string }> = [];

	options.push(
		...guild.categories
			.filter((cat) => !ignore.some((k) => k === cat.id))
			.map((cat) => ({
				label: `${cat.name} (category)`,
				value: cat.id,
				group: "Categories",
			})),
	);

	// Filter these channels
	const channelsWithoutCategory = guild.channels_without_category.filter(
		(c) => c.can_redirect_from && !ignore.some((k) => k === c.id),
	);
	if (channelsWithoutCategory.length > 0)
		options.push(
			...channelsWithoutCategory.map((c) => ({
				value: c.id,
				label: `#${c.name}`,
				group: "(no category)",
			})),
		);

	options.push(
		...guild.categories.flatMap((cat) =>
			cat.channels
				.filter((c) => c.can_redirect_from && !ignore.some((k) => k === c.id))
				.map((c) => ({ value: c.id, label: `#${c.name}`, group: cat.name })),
		),
	);

	return options;
};
