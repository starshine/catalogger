# Terms of Service

By using the Catalogger Discord bot (henceforth referred to as “the bot”) or the Catalogger dashboard (henceforth referred to as “the website”), you accept and agree to abide by the terms and provisions of this agreement.

In addition, when using the bot’s services, or those of the website, you shall be subject to any posted guidelines or rules applicable to such services, which may be posted and modified from time to time.

All such guidelines or rules are hereby incorporated by reference into the Terms Of Service (henceforth referred to as ‘the ToS’).

ANY USE OF THE BOT OR THE WEBSITE CONSTITUTES AGREEMENT TO THESE TERMS OF SERVICE. IF YOU DO NOT AGREE TO THE TOS, DO NOT USE THE BOT OR THE WEBSITE.

The service is offered in the hopes that it will be useful, but without any warranty. The developer shall not be responsible or liable for the accuracy, correctness, usefulness or availability of any information transmitted or made available via bot or the website or any other officially associated media, and shall not be responsible or liable for any error or omissions in that information. The developer is also not liable for any damage, data loss or harm caused by the use or misuse of the bot or the website, however unlikely that may be. By using the bot or the website, you agree to this and you understand that the developer is not liable for damage, data loss or other harm, caused by the use or misuse of the bot or the website, or association with the developer in any other way.

The developer reserves the right to modify, remove or revoke access to the service, whether to discontinue it as a whole or bar specific persons or parties from accessing or using the bot, the website, or any other officially associated media for any reason, including but not limited to:
deliberately interfering with the service’s operation, such as by attempting a denial of service attack;
using the service to break the law;
using the service to break other organizations’ Terms of Service, including Discord.

The developer will take whatever steps are necessary to preserve the integrity of the software and hardware the service runs upon, and again reserves the right to bar and take any reasonable measure against parties or persons who attempt to prevent that. In the event of a data breach, the developer will handle it in accordance with the law.

Catalogger is proud to have good privacy practices and is GDPR compliant. You can find the privacy policy [here](/about/privacy).

If you believe your intellectual property rights have been infringed by the developer, please contact us [here](/about/contact).
