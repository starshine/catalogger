# Contact

The best way to contact us is through [the support server](https://discord.gg/b5jjJ96Jbv).

If you would rather use email, please send an email to `catalogger [at] starshines [dot] gay`.
