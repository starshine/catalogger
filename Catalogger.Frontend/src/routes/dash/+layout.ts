import { addToast } from "$lib/toast";
import { redirect } from "@sveltejs/kit";

export const prerender = false;

export const load = async ({ parent }) => {
	const data = await parent();
	if (!data.user) {
		addToast({ body: "You are not logged in." });
		redirect(303, "/");
	}

	return { user: data.user!, guilds: data.guilds! };
};
