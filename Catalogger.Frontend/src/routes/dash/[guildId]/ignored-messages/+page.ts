import apiFetch from "$lib/api";

export const load = async ({ params }) => {
	const users = await apiFetch<Array<{ id: string; tag: string }>>(
		"GET",
		`/api/guilds/${params.guildId}/ignored-users`,
	);

	return { users };
};
