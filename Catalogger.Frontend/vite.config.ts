import { sveltekit } from "@sveltejs/kit/vite";
import { defineConfig } from "vite";
import { Mode, plugin as markdown } from "vite-plugin-markdown";

export default defineConfig({
	plugins: [sveltekit(), markdown({ mode: [Mode.HTML] })],
	server: {
		port: 5173,
		strictPort: true,
		proxy: {
			"/api": {
				target: "http://localhost:5005",
				changeOrigin: true,
			},
		},
		hmr: {
			host: "localhost",
			port: 5173,
			protocol: "ws",
		},
	},
});
